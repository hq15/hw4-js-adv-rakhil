// AJAX это набор технологий позволяющий отправлять запросы и получать ответы в фоновом режиме средствами JS. Вследствии  этого мы получаем возможность в JS принять решение что сделать с этим ответом и без перегрузки страницы. Используюутся технологии:  JS на стороне клиента, HTTP, на сервере серверный язык (к примеру PHP), HTML, CSS и ХML.

const requestURL = "https://ajax.test-danit.com/api/swapi/films";
const movieList = fetch(requestURL).then((response) => response.json());
const block = document.getElementById("root");
movieList.then((data) => {
  console.log(data);
  showMovieList(data);
});

function showMovieList(arr) {
  const arrList = [];
  arr.forEach(function (item) {
    const itemId = `"${item.id}"`;
    list = document.createElement("ul");
    block.prepend(list);
    item.characters.forEach((link) => {
      fetch(link)
        .then((response) => response.json())
        .then((character) => {
          const itemPerson = document.createElement("li");
          itemPerson.innerText = ` ${character.name},`;
          document.querySelector(`[data-movie-id=${itemId}]`).append(itemPerson);
        });
    });
    let stringMovie;
    stringMovie = `<h4>Номер эпизода: ${item.episodeId}</h4><li>Название фильма: "${item.name}",<ul data-movie-id="${item.id}">Персонажи:</ul></li><li>Короткое содержание: ${item.openingCrawl}</li>`;
    arrList.push(stringMovie);
  });
  list.insertAdjacentHTML("beforeend", `${arrList.join("")}`);
};
